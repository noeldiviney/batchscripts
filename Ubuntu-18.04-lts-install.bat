@echo off
REM Test if VcXsrv,exe is running and if not Launch it
REM Launch Ubuntu-18.04-W2 with selected user-name

set DISTRO_MASTER=Ubuntu-18.04
set DISTRO_MASTER_TARBALL=ubuntu-18.04.tar.gz
set DISTRO_NEW=Ubuntu-18.04-W2
set DISTRO_NEW_LOCATION=ubuntu-18.04-W2

echo "Line 10    We are here"

set c=
echo "Line 13    Creating                %DISTRO_MASTER_TARBALL%"

:CheckTarballExists
IF EXIST "%DISTRO_MASTER_TARBALL%" ( 
	echo "Line 30    goto                    :tarball_found "
	goto :tarball_found	
) else (
	echo "Line 20    goto                    :tarball_not_found "
    goto :tarball_not_found
)

:tarball_found
    echo "Line 25    Tarball                 %DISTRO_MASTER_TARBALL% exists"
    echo "Line 26    c = %c%"
    echo -------- 
    set /P c=              Delete  %DISTRO_MASTER_TARBALL% [Y/N]?
    if /I "%c%" EQU "y" (
        echo "Line 30    Delete?                Yes "
	    echo --------
        echo "Line 32    Deleting               %DISTRO_MASTER_TARBALL% "
::		del %DISTRO_MASTER_TARBALL%
	    echo "Line 34    goto                   create_tarball "
		goto :create_tarball
    )
	if /I "%c%" EQU "n" (
	    echo --------
	    echo "Line 39    Delete?                 No "
		echo "Line 40    Keeping                 Existing %DISTRO_MASTER_TARBALL% "
		echo "Line 41    goto                    :install_distro "
		goto :install_distro
	)
	
:tarball_not_found
    echo "Line 44    Tarball                 %DISTRO_MASTER_TARBALL% does not exist"
	echo "Line 45    goto                    :create_tarball "
    goto :create_tarball

:create_tarball
	echo "Line 51    Creating                %DISTRO_MASTER_TARBALL% "
    echo "Line 52    Executing               wsl --export %DISTRO_MASTER% %DISTRO_MASTER_TARBALL%"
    wsl --export %DISTRO_MASTER% %DISTRO_MASTER_TARBALL%
    IF EXIST "%DISTRO_MASTER_TARBALL%" (
        echo "Line 55    Tarball           %DISTRO_MASTER_TARBALL% successfully created"
    ) else (
        echo "Line 57    Tarball           %DISTRO_MASTER_TARBALL% not created"
		echo "Line 58    goto                    :exit "
		goto :exit
    )	
	echo "Line 61    goto                    :install_distro "
    goto :install_distro

:install_distro
    if exist "%DISTRO_NEW_LOCATION%" (
        echo "Line 66    Distro                  %DISTRO_NEW_LOCATION% exists"
		echo "Line 67    goto                    :distro_unregister "
		goto :distro_unregister
	)
    echo "Line 70    Installing              %DISTRO_NEW% to %DISTRO_NEW_LOCATION% "
    echo "Line 71    Executing               wsl --import %DISTRO_NEW% %DISTRO_NEW_LOCATION% %DISTRO_MASTER_TARBALL%"
    wsl --import %DISTRO_NEW% %DISTRO_NEW_LOCATION% %DISTRO_MASTER_TARBALL%
    IF EXIST "%DISTRO_NEW%" (
        echo "Line 74    Distro                  %DISTRO_NEW% successfully installed"
    ) else (
        echo "Line 76    Distro                  %DISTRO_NEW% not installed"
		echo "Line 77    goto                    :exit "
		goto :exit
    )	
	echo "Line 80    goto                    :distro_upgrade_wsl2 "
    goto :distro_upgrade_wsl2

:distro_unregister
	echo "Line 84    Executing               wsl --unregister %DISTRO_NEW% "
	wsl --unregister %DISTRO_NEW%
	echo "Line 86    Executing               rmdir /s /q %DISTRO_NEW_LOCATION% "
	rmdir /s /q %DISTRO_NEW_LOCATION%
	echo "Line 88    goto                    :install_distro "
	goto :install_distro
    
:distro_upgrade_wsl2
    echo "Line 92    Executing               wsl --set-version %DISTRO_NEW% 2"
	wsl --set-version %DISTRO_NEW% 2
	echo "Line 94    goto                    :launch_distro "
    goto :launch_distro
    
:launch_distro
    SET MyProcess=VcXsrv.exe
    ECHO "Line 99    Test if                 %MyProcess% is running"
::goto :exit
    TASKLIST | FINDSTR /I "%MyProcess%">nul
    if %errorlevel% neq 0 (
        ECHO "Line 103   X Server                %MyProcess% is not running"
	    echo "Line 104   start W:\ProgramFiles\VcXsrv\vcxsrv.exe -ac -terminate -lesspointer -multiwindow -clipboard -wgl"
        start W:\ProgramFiles\VcXsrv\vcxsrv.exe -ac -terminate -lesspointer -multiwindow -clipboard -wgl
        echo "Line 106   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-18.04-W2 -u noel "
        start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-18.04-W2 -u noel
        goto :exit
    ) ELSE (
        ECHO "Line 110   X Server                %MyProcess% is running
        echo "Line 111   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-18.04-W2 -u noel "
        start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-18.04-W2 -u noel
        goto :exit
)	

:exit
	echo "Line 104   Exiting "
	exit /B

  echo "Line 18    Tarball           %DISTRO_MASTER_TARBALL% exists"
  set /P c= Line 19    Delete             %DISTRO_MASTER_TARBALL% [Y/N]?
::  if /I "%c%" EQU  "y" (
::    :choice
    if /I "%c%" EQU "Y" (
      echo "Line 21    Yes               %c% Deleting %DISTRO_MASTER_TARBALL% "
	  del %DISTRO_MASTER_TARBALL%
      echo "Line 23    Executing         wsl --export %DISTRO_MASTER% %DISTRO_MASTER_TARBALL%"
      wsl --export %DISTRO_MASTER% %DISTRO_MASTER_TARBALL%
      IF EXIST "%DISTRO_MASTER_TARBALL%" (
          echo "Line 26    Tarball           %DISTRO_MASTER_TARBALL% successfully created"
      )	  
    ) else (
      echo "Line 29    No               %c% Continue useing %DISTRO_MASTER_TARBALL% "      
  )
) ELSE (
  echo "Line 32    Tarball           %DISTRO_MASTER_TARBALL% does not exist"
  echo "Line 33    Executing         wsl --export %DISTRO_MASTER% %DISTRO_MASTER_TARBALL%"
  wsl --export %DISTRO_MASTER% %DISTRO_MASTER_TARBALL%
  IF EXIST "%DISTRO_MASTER_TARBALL%" (
      echo "Line 36    Tarball           %DISTRO_MASTER_TARBALL% successfully created"
  )	  
)
echo "Line 39    Installing              %DISTRO_NEW% to %DISTRO_NEW_LOCATION% "
echo "Line 40    Executing               wsl --import %DISTRO_NEW% %DISTRO_NEW_LOCATION% %DISTRO_MASTER_TARBALL%"

