@echo off
REM Install Enable WSL features
SET VERSION=2004

dism.exe /online /Disable-Feature /FeatureName:VirtualMachinePlatform /norestart
dism.exe /online /Disable-Feature /FeatureName:Microsoft-Windows-Subsystem-Linux
