@echo off
REM Install Ubuntu distro
SET VERSION=2004
cd W:\WSL
echo "cwd = %cd%"
ECHO "Test if wslubuntu%2004% is already downloaded"
if EXIST "Ubuntu-%VERSION%".appx (
	echo "Ubuntu-%VERSION%.appx exists"
) else (
	echo "Downloading ubuntu-%VERSION%.appx"
	curl.exe -L -o Ubuntu-%VERSION%.appx https://aka.ms/wslubuntu%VERSION%
)
echo "Installing Ubuntu-%VERSION%.appx"
Ubuntu-2004.appx
		
