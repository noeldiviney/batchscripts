@echo off
REM Test if VcXsrv,exe is running and if not Launch it
REM Launch Ubuntu-18.04-W with selected user-name
SET MyProcess=VcXsrv.exe
ECHO "Test if %MyProcess% is running"
TASKLIST | FINDSTR /I "%MyProcess%">nul
if %errorlevel% neq 0 (
   ECHO "%MyProcess% is not running"
   start W:\ProgramFiles\VcXsrv\vcxsrv.exe -ac -terminate -lesspointer -multiwindow -clipboard -wgl
   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-18.04-W -u noel
   EXIT /B
) ELSE (
   ECHO "%MyProcess%" is running
   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-18.04-W -u noel
   EXIT /B
)	
